# -*- coding: utf-8 -*-

# здесь ничего менять не надо 
from parser import parseData
from params import *
__all__ = ['parseData', 'DBNAME', 'USER', 'PASSWORD', 'SCHEMA', 'TABLE', 'FIELDSNAME',
			'SOURCEURL', 'PATH2FILE', 'CSVFILENAME', 'XPATH2SOURCEDATA', 'HASHKEY', 'HASHFILENAME',
			'LOGFILENAME', 'ERRORFILENAME', 'SOURCETYPE']
