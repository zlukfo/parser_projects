# -*- coding: utf-8 -*-

# источник данных
SOURCEURL='http://zakupki.gov.ru/epz/order/extendedsearch/rss'
SOURCETYPE='xml'	# тип источника. допустимые значения 'xml', 'html'

# список xpath путей к извлекаемым данным. Подробности здесь .....
XPATH2SOURCEDATA=['item/description']

# параметры подключения к таблице БД в которую сохраняются извлекаемые данные
DBNAME='parser'
USER='postgres'
PASSWORD='qweasd'
SCHEMA='zakupki'	# если схема public - ее тоже указывать
TABLE='zakupki_rss'

# имена полей таблицы TABLE. Тип tuple определяет последовательность, в которой должна расставить
# данные функция parser.py -> parseData()  
FIELDSNAME=("fz", "currency", "district", "number", "link", "description", "customer",  "price", "create_date")

# список полей (из FIELDSNAME) которые определяют уникальность каждой записи
# если нужно добавлять все записи (проверка на уникальность не нужна) - пустой список 
HASHKEY=["number", "create_date"]	

# путь к каталогу, в котором будут созданы файлы, необходимые для работы парсера, например '/tmp'
PATH2FILE='/tmp'

# ЗАРЕЗЕРВИРОВАННЫЕ ИМЕНА ПЕРЕМЕННЫХ (заполнять не нужно)
CSVFILENAME=''
HASHFILENAME=''
LOGFILENAME=''
ERRORFILENAME=''
