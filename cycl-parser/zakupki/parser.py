# -*- coding: utf-8 -*-
from params import *
from lxml import etree
def parseData(d):
	data=[None]*len(FIELDSNAME)
	
	root=etree.HTML(d)
	if root is None:
		return []
	context = etree.iterwalk(root, events=("start","end"), tag=["strong", "b", "a"])
	for action, elem in context:
		if action=='end':
			if elem.text and elem.text.find(u'Размещение выполняется')!=-1:
				data[0]=elem.tail #or 'None'
			if elem.text and elem.text.find(u'Валюта')!=-1:
				data[1]=elem.tail #or 'None'
			if elem.text and elem.text.find(u'Субъект РФ')!=-1:
				data[2]=elem.tail #or 'None'
			# номер контракта
			if elem.text and elem.text.isdigit():
				data[3]=elem.text #or 'None'
			# ссылка на контракт
			if elem.values():
				data[4]=elem.values()[0] #or 'None'				
			# описание контракта
			if elem.text and not elem.tail and not elem.values():
				data[5]=elem.text #or 'None'
			if elem.text and elem.text.find(u'Наименование Заказчика')!=-1:
				data[6]=elem.tail #or #'None'
			if elem.text and elem.text.find(u'Начальная цена контракта')!=-1:
				data[7]=elem.tail #or '0.0'
			if elem.text and elem.text.find(u'Размещено')!=-1:
				data[8]=elem.tail #or 'NULL'				
	return data

