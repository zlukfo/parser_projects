#!/usr/bin/python
# -*- coding: utf-8 -*-
from xmlIterParser import xmlParser
#from classParserError import *
import utilFunc
import codecs
import re
import csv
import time
import os
import datetime
import sys
import shutil
from daemonize import daemonize



import psycopg2
from psycopg2.extensions import adapt
import hashlib

def getConnection():
	'''
	Function: getConnection
	Summary: подключается к бд, указанной в настройках
	'''
	if not all([DBNAME, USER, PASSWORD]):
		sys.stderr.write('\nError: Not all connection params is define ...\n\n')
		return None
	try:
		conn = psycopg2.connect("dbname='%s' user='%s' password='%s'" % (DBNAME, USER, PASSWORD))
		conn.autocommit=True
		cur = conn.cursor()
	except:
		sys.stderr.write('\nError: Connection on db fail ...\n\n')
		return	None
	return (conn, cur)


def saveRecord2Db(cur, values=[]):
	'''
	Function: saveRecord2Db
	Summary: Сохраняет ОДНУ запись в бд
	'''
	if not values:
		return None
	try:
		values=[adapt(f).getquoted() for f in values]	
		cur.execute("INSERT  INTO \"%s\".\"%s\" (%s) VALUES (%s);" % (SCHEMA, TABLE, ', '.join(FIELDSNAME), ", ".join(values)))
	except psycopg2.Error as e:
		sys.stderr.write('Insert record: %s, %s\n' %(e.pgcode, e.pgerror))

def _getStringFromEndFile(filename, string_len, delimiter_line_len=1, emty_line_in_end=True):
	'''
	Function: _getStringFromEndFile
	Summary: считывает по одной строке с конца файла (в данном проекте - файла хеша)
	Attributes: 
		@param (filename): имя файла
		@param (string_len): длина строки (хеша) - ФИКСИРОВАННАЯ
		@param (delimiter_line_len) default=1: длина разделителя строк (для файла хеша - '\n' - 1)
		@param (emty_line_in_end) default=True: заканчивается ли файл пустой строкой
	Returns: генератор на строки файла
	'''
	if not os.path.exists(filename):
		return
	with codecs.open(filename, 'r', 'utf-8') as hk:
		if emty_line_in_end:
			hk.seek(0,2)
		else:
			hk.seek(1,2)
		while hk.tell()>string_len:
			hk.seek(-(string_len+delimiter_line_len),1)
			yield hk.read(string_len)
			hk.seek(-string_len,1)
	yield None

def getDataFromSource():
	'''
	Function: getDataFromSource
	Summary: обращается к источнику, собирает все записи и добавляет их в файл CSVFILENAME
			работает в двух режимах
			1) если HASHKEY не определен - при каждом обращении к источнику собираются ВСЕ записи
			и затем добавляются в бд
			2) в HASHKEY задано одно или несколько полей из FIELDSNAME - хеш по этим полям считается
			уникальным идентификатором записи. при обращении каждая запись из источника проверяется
			на уникальность по хешу и добавляется в CSVFILENAME только если хеш уникальный
	Returns: InsertHere
	'''
	x=xmlParser(SOURCEURL)
	# !!! расширить - есть возможность еще извлекать данные из атрибутов
	gd=x.getData(XPATH2SOURCEDATA)

	hk=codecs.open(HASHFILENAME, 'a', 'utf-8')
	with codecs.open(CSVFILENAME, 'a', 'utf-8') as f:
		count = 0
		for i in gd:
			data=parseData(i[1])
			if not data: continue			
			#!!! добавить искт если количество полей в ксв все таки не совпадает с бд
			# подготавливаем каждое поле записи
			for k in xrange(len(data)):
				data[k]='None' if type(data[k])==type(None)	else data[k]
				data[k]=re.sub(ur'[\n\t\r;]+', '',data[k])
			if not HASHKEY:
				f.write(';'.join(data)+'\n')
				count+=1
			else:
				s=''.join([data[FIELDSNAME.index(j)] for j in HASHKEY])				
				h=hashlib.sha224(s.encode('utf-8')).hexdigest()		
				find_flag=False
				for line in _getStringFromEndFile(HASHFILENAME, 56):
					if line==h:
						find_flag=True
						break
				if find_flag: continue
				hk.write(h+'\n')
				f.write(';'.join(data)+'\n')
				count+=1
		sys.stdout.write('%s New %d rows.\n' % (str(datetime.datetime.now()),count))
	hk.close()
	

def selectProject(project):
	'''
	Function: selectProject
	Summary: проверяет существование проекта и корректность его конфигурации
			если проект (каталог) существует - добавляет его в окружение поиска путей
	'''
	if project not in [i[1] for i in os.walk('.')][0]:
		print '\nError: Project is not exists ...\n'
		return None
	for i in [(i[0],i[2]) for i in os.walk('.')]:
		if './'+project==i[0] and set(['params.py', 'parser.py', '__init__.py'])<=set(i[1]):
			mod = __import__(project)
			globals().update({k: getattr(mod, k) for k in mod.__all__})
			return 1			
	print '\nError: Config project is not correct ...\n'
	return None

# ФУНКЦИИ, ОБРАБАТЫВАЮЩИЕ КОМАНДЫ, ЗАДАННЫЕ ПРИ ЗАПУСКЕ ПАРСЕРА	
def createProject(project_name):
	'''
	Function: createProject
	Summary: выполняется при запуске парсера с командой create. Создает папку 'project_name'
			и копирует туда из _template файлы-шаблоны, необходимые для работы проекта
	'''
	if os.path.exists(project_name):
		if os.path.isdir(project_name):
			print '\nError: Project "%s" exists ...\n' % (project_name,)
			return 0
	shutil.copytree('./_template', './%s' % (project_name,))
	print '\nProject "%s" created!\n' % (project_name,)
	return 1

def getTree(project_name, tree_format='png'):
	'''
	Function: getTree
	Summary: выполняется при указании команды get_tree. Анализирует структуру источника
			и сохраняет ее в виде дерева в графический файл 
	'''
	if not SOURCEURL or not SOURCETYPE:
		print '\nError: Params SOURCEURL, SOURCETYPE must defined ...\n'
		return 0
	if tree_format not in ['png', 'svg']:
		print '\nError: Save tree to "%s" is not available. Use "png" or "svg" ...\n' % (tree_format,)
		return 0
	if SOURCETYPE not in ['xml', 'html']:
		print '\nError: Source format "%s" is not available. Use "xml" or "html" ...\n' % (SOURCETYPE,)
		return 0
	try:
		x=xmlParser(source_path=SOURCEURL, source_type=SOURCETYPE)
	except:
		print '\nError: create source object. May be SOURCEURL or SOURCEURL not defined ...\n'
		return 0
	x.getTree()
	if tree_format=='png':
		x.saveTree2png(file_output=project_name+'/tree')
	if tree_format=='svg':
		x.saveTree2svg(file_output=project_name+'/tree')
	return 1

def testXpath(project_name):
	'''
	Function: testXpath
	Summary: выполняется при указании команды test_xpath. Выводит на консоль первые 10 (или меньше)
			объектов (записей) списка данных извлеченных из источника
	'''
	if not SOURCEURL or not XPATH2SOURCEDATA or not SOURCETYPE:
		print 'Params SOURCEURL, XPATH2SOURCEDATA, SOURCETYPE must defined ...'
		return 0
	if SOURCETYPE not in ['xml', 'html']:
		print '\nError: Source format "%s" is not available. Use "xml" or "html" ...\n' % (SOURCETYPE,)
		return 0
	try:
		x=xmlParser(source_path=SOURCEURL, source_type=SOURCETYPE)
	except:
		print '\nError: create source object ...\n'
		return 0
	x.getTree()
	# !!! расширить - есть возможность еще извлекать данные из атрибутов
	gd=x.getData(XPATH2SOURCEDATA, head=10)
	c=0
	for i in gd:
		if c>=10:break
		if type(i[1])==type({}):
			for k,v in i[1].items():
				print '"%s": %s' % (k, v)
			print '--------------------------------------'
		else: print i[1]
		c+=1 	
	return 1

def getPath(project_name, tagname='', attrname='', attrval=''):
	'''
	Function: getPath
	Summary: выполняется, если указана команда get_path. возвращает список xpath путей с заданными
			атрибутами
	'''
	try:
		x=xmlParser(source_path=SOURCEURL, source_type=SOURCETYPE)
	except:
		print '\nError: create source object ...\n'
		return 0
	# !!! обязательно реализовать построение дерева с возможностью атрибутов и их значениями
	return utilFunc.getPathList(x.getTree(), tagname=tagname, attrname=attrname, attrval=attrval)

import optparse
if __name__=='__main__':
	print '\nRun parser command: ', (sys.argv)	
	p=optparse.OptionParser(usage='%prog -[commands] project_name')
	p.add_option("-c", "--create", action="store_true", dest="create", help="Create new project")
	p.add_option("-t", "--tree", dest="gettree", type="choice", choices=['png', 'svg'], help="Save structura source as tree to 'png' or 'svg' file", metavar='png|svg')
	p.add_option("-x", "--testxpath", action="store_true", dest="testxpath", help='Return 10 first record')
	p.add_option("-p", "--path", dest="getpath", help="Rectore xpath to data from tagname, or attrname, or attrval", metavar="\"tagname='', attrname='', attrval=''\"")
	p.add_option("-d", "--daemon", action="store_true", dest="daemon", help='Run parser in daemon, default - false')
	p.add_option("-i", "--interval", action="store", type=int, default=(120,60), dest="interval", nargs=2, help='Cycl period (val1), save period (val2)', metavar="val1 val2")
	opt,args=p.parse_args()
	#print opt,args
	# проверяем,чтобы задан был только один проект
	if len(args)!=1:
		print 'Parser work with ONE project ...\n'
		quit()

	project_name=args[0]
	if opt.create:
		createProject(project_name)
		quit()
	selectProject(project_name)
	if opt.gettree:
		getTree(project_name, opt.gettree)
		quit()
	if opt.testxpath:
		testXpath(project_name)
		quit()
	if opt.getpath:
		param={'tagname':'', 'attrname':'', 'attrval':''}
		opt.getpath=re.split('[, ]+',opt.getpath)
		opt.getpath=dict([i.split('=')[:2] for i in opt.getpath if '=' in i])
		for k,v in opt.getpath.items():
			if k in param:
				param[k]=v
		print '\nXpath find params: ', param
		print 'Find xpath: ', getPath(project_name, tagname=param['tagname'], attrname=param['attrname'], attrval=param['attrval']), '\n'
		quit()

	period=opt.interval[0]	# интервал чере который данные из текстового файла страбываются в бд
	sleep=opt.interval[1]	# интервал, через который выполняется обращение к источнику для получения данных

	if not PATH2FILE:
		print '\nError: Set param PATH2FILE ...\n'
		quit()
	CSVFILENAME=PATH2FILE+'/'+project_name+'.csv'
	HASHFILENAME=PATH2FILE+'/'+project_name+'.key'
	LOGFILENAME=PATH2FILE+'/'+project_name+'.log'
	ERRORFILENAME=PATH2FILE+'/'+project_name+'.err'

	if HASHKEY and not(set(HASHKEY)<=set(FIELDSNAME)):
		print 'HASHKEY define but not correct'
		quit()
	
	# хеш может не использоваться, но файл хеша должен сущестровать
	if not os.path.exists(HASHFILENAME):
		f=open(HASHFILENAME,'w')
		f.close()

	if opt.daemon:
		daemonize(stdout=LOGFILENAME, stderr=ERRORFILENAME)

	conn, cur=getConnection() or (None, None)
	if not conn or not cur:
		quit()

	start=datetime.datetime.now()

	while True:
		nn=datetime.datetime.now()
		if (nn-start).seconds>=period:
			with open(CSVFILENAME, 'rU') as csvfile:
				for record in csv.reader(csvfile, delimiter=';'):
					saveRecord2Db(cur, record)
			#файл накапливает данные пока они не будут сохранены в бд. после сохранения он очищается
			f=open(CSVFILENAME,'w')
			f.close()
			
			start=datetime.datetime.now()

		# 2) полученные от источника данные могут поменять структуру 
		# 3) источник может заблокировать доступ парсера
		# В этом случае возбуждается исключение
		try:
			getDataFromSource()
		except Exception as e:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
			sys.stderr.write('%s getDataFromSource() error: type: %s, file: %s, line: %d\n' % (str(datetime.datetime.now()), str(exc_type), fname, exc_tb.tb_lineno))

		time.sleep(float(sleep))
	
	conn.close()