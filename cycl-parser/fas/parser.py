# -*- coding: utf-8 -*-
from params import *
import re
def parseData(d):
	data=[None]*len(FIELDSNAME)
	for k,v in d.items():
		v=v.replace('\n','')
		v=re.sub(r'<.*>','',v)
		if k in FIELDSNAME:
			data[FIELDSNAME.index(k)]=v			
	return data


