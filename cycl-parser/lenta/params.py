# -*- coding: utf-8 -*-

# источник данных
SOURCEURL='https://lenta.ru/rss/news'
SOURCETYPE='xml'	# тип источника. допустимые значения 'xml', 'html'

# список xpath путей к извлекаемым данным. Подробности здесь .....
XPATH2SOURCEDATA=['channel/item']

# параметры подключения к таблице БД в которую сохраняются извлекаемые данные
DBNAME='parser'
USER='postgres'
PASSWORD='qweasd'
SCHEMA='zakupki'	# если схема public - ее тоже указывать
TABLE='lenta'

# имена полей таблицы TABLE. Тип tuple определяет последовательность, в которой должна расставить
# данные функция parser.py -> parseData()  
FIELDSNAME=("link", "title", "description", "pubDate",  "category")

# список полей (из FIELDSNAME) которые определяют уникальность каждой записи
# если нужно добавлять все записи (проверка на уникальность не нужна) - пустой список 
HASHKEY=['title', 'pubDate']

# путь к каталогу, в котором будут созданы файлы, необходимые для работы парсера, например '/tmp'
PATH2FILE='/tmp'

# ЗАРЕЗЕРВИРОВАННЫЕ ИМЕНА ПЕРЕМЕННЫХ (заполнять не нужно)
CSVFILENAME=''
HASHFILENAME=''
LOGFILENAME=''
ERRORFILENAME=''
