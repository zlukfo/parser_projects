#!/usr/bin/python
# -*- coding: utf-8 -*-
from xmlIterParser import xmlParser
import codecs
import re
import csv
import time
import psycopg2
import os
import datetime
import sys
from daemonize import daemonize

def save2db(filename):
	tbl="zakupki.zakupki_rss"
	try:
		conn = psycopg2.connect("dbname='parser' user='postgres' password='qweasd'")
		conn.autocommit=True
		cur = conn.cursor()
	except:
		#print 'Not connection to db'
		sys.stderr.write('Not connection to db')
		#sys.stdout.flush()
		return
	
	f=open(filename)
	reader = csv.reader(f, delimiter=';')
	for i in reader:
		i[0]=re.sub('[^\d]+','', i[0])
		if not i[0]:continue
		i[1]=re.sub('(\d\d)\.(\d\d)\.(\d\d\d\d)',r'\3-\2-\1' ,i[1])
		if not i[2] or i[2]=='None': i[2]=0

		i[3]=re.sub('(</st)|(&[a-z]+,)','', i[3])
		q="INSERT INTO %s (\"number\",create_date,price,description,type,fz,link) VALUES (%s,'%s',%s,'%s','%s','%s','%s');" %(tbl, i[0],i[1],i[2],i[3],i[4],i[5],i[6])
		
		try:
			# ключевое поле - номер, строки с одинаковым ключевым полем добаляться не будут
			cur.execute(q)
		except:
			pass
			#print 'Record not add to db'
			#sys.stdout.flush()
	conn.close()	
	f.close()
	os.remove(filename)



def getRss():	
	base_url='http://zakupki.gov.ru/epz/order/extendedsearch/rss'
	x=xmlParser(base_url)

	f=codecs.open('/tmp/zakupki_new.csv', 'a', 'utf-8')

	ff=open('/tmp/zakupki_new.csv')
	reader = csv.reader(ff, delimiter=';')
	n=[i[0] for i in reader]
	ff.close()

	count = 0
	gd=x.getData(['item/description'])

	for i in gd:
		data={}
		s=re.split(r'<br/?>',i[1])[8:]
		if len(s)<6:continue
		
		data['type']=re.sub(r'<[a-z/]+>', '', s[0])
		data['type'], data['number']=re.split(r'<.*>', data['type'])


		if data['number'].encode('utf-8') in n: continue

		data['href']=''
		h=re.search(r"='.*' target", s[1])		
		if h:
			data['href']=h.group(0)[2:-8]
		data['title']=re.search(r"<strong>.*(</strong>)?", s[1]).group(0)[8:-9]
		data['title']=data['title'].replace(';',',')
		data['fz']=re.search(r"</strong>.*$", s[2]).group(0)[9:]
		data['customer']=re.search(r"</strong>.*$", s[3]).group(0)[9:]
		t=re.search(r"</strong>.*<strong>", s[4])
		data['price']='None'
		if t: data['price']=t.group(0)[9:-8]
		data['date']=re.search(r"</strong>.*$", s[5]).group(0)[9:]
		f.write('%s;%s;%s;%s;%s;%s;%s\n' %(data['number'], data['date'],data['price'],data['title'],data['type'],data['fz'],data['href']))
		count+=1



	f.close()
	print '%s New %d rows.' % (str(datetime.datetime.now()),count)
	sys.stdout.flush()
	sys.stdout.write('%s New %d rows.\n' % (str(datetime.datetime.now()),count))

if __name__=='__main__':
	daemonize(stdout='/tmp/zakupki_parser.log', stderr='/tmp/zakupki_error.log')
	period=120	# первый параметр в командной строке запуска (интервал чере который данные из текстового файла страбываются в бд)
	sleep=60	# второй параметр в командной строке запуска (интервал, через который выполняется обращение к источнику для получения данных)
	if len(sys.argv)>=2:
		period=int(sys.argv[1])
	if len(sys.argv)>=3:
		sleep=int(sys.argv[2])
	print sys.argv
	sys.stdout.flush()	

	start=datetime.datetime.now()

	while True:
		nn=datetime.datetime.now()
		if (nn-start).seconds>period: 
			save2db('/tmp/zakupki_new.csv')
			start=datetime.datetime.now()

		getRss()
		time.sleep(float(sleep))
