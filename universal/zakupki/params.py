# -*- coding: utf-8 -*-
# настройки подключения к БД
DBNAME='parser'
USER='postgres'
PASSWORD='qweasd'
# настройки таблиц для сохранения данных
SCHEMA='zakupki'
#TABLE='zakupki_rss'
TABLE='lenta'

#FIELDSNAME=("fz", "currency", "district", "number", "link", "description", "customer",  "price", "create_date")
FIELDSNAME=("link", "title", "description", "pubDate",  "category")


#SOURCEURL='http://zakupki.gov.ru/epz/order/extendedsearch/rss'
SOURCEURL='https://lenta.ru/rss/news'

#CSVFILENAME='/tmp/zakupki_new.csv'
CSVFILENAME='/tmp/lenta.csv'

#XPATH2SOURCEDATA=['item/description']
XPATH2SOURCEDATA=['channel/item']