# -*- coding: utf-8 -*-
# настройки подключения к БД
DBNAME='parser'
USER='postgres'
PASSWORD='qweasd'
# настройки таблиц для сохранения данных
SCHEMA='zakupki'
#TABLE='zakupki_rss'
TABLE='lenta'

#FIELDSNAME=("fz", "currency", "district", "number", "link", "description", "customer",  "price", "create_date")
FIELDSNAME=("link", "title", "description", "pubDate",  "category")
HASHKEY=['title', 'pubDate']	# если использовать проверку по хешу на планируется - пустой масств 

#SOURCEURL='http://zakupki.gov.ru/epz/order/extendedsearch/rss'
SOURCEURL='https://lenta.ru/rss/news'
PATH2FILE='/tmp'


#XPATH2SOURCEDATA=['item/description']
XPATH2SOURCEDATA=['channel/item']

# эти имена переменных зарезервированы для использования в скрипте
CSVFILENAME=''
HASHFILENAME=''
LOGFILENAME=''
ERRORFILENAME=''
