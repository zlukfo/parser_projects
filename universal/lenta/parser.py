# -*- coding: utf-8 -*-
from params import *
# *** вот эта функция у каждого проекта будет своя
# должна возвращать список значений полей ОДНОЙ добавляемой записи или пустрой массив
# значения списка должны быть остотированы в соотвествии с расположением полей переменной FIELDSNAME
def parseData(d):
	data=['None']*len(FIELDSNAME)
	for k,v in d.items():
		v=v.replace('\n','')
		if k in FIELDSNAME:
			data[FIELDSNAME.index(k)]=v			
	return data

