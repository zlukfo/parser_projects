# -*- coding: utf-8 -*-
#текущий проект - ремонт и строительство
from selenium import webdriver
from pyvirtualdisplay import Display 

LIMIT_PAGE=100	#максимальное количество страниц на ссылки объявлений разрешаемое avito

f=open('remont_stroitelstvo2.txt', 'w')

#subcategory=[
#	'sborka_i_remont_mebeli', 'melkiy_bytovoy_remont','elektrika','santekhnika',
#	'remont_ofisa',	'ostekleniye_balkonov',	'remont_vannoy', 'stroitelstvo_ban_saun',
#	'remont_kuhni',	'stroitelstvo_domov_kottedzhey','remont_kvartiri'
#]
subcategory=[
	'remont_kvartiri', 'remont_ofisa', 'stroitelstvo_ban_saun', 'santekhnika',
	'remont_vannoy', 'remont_kuhni',	'stroitelstvo_domov_kottedzhey'
]


link_generator=('https://m.avito.ru/'+region+'/predlozheniya_uslug/remont_stroitelstvo/'+subcat for region in open('avito_regions.txt','r') for subcat in subcategory)


# http://toddhayton.com/2015/02/03/scraping-with-python-selenium-and-phantomjs/
display = Display(visible=0, size=(800, 600))
display.start()
driver = webdriver.Firefox()

#driver = webdriver.PhantomJS()
#driver.set_window_size(800, 600)
for link in link_generator:
	print link
	driver.get(link)
	elems=driver.find_elements_by_class_name("item-link")
	# cсылки на объявления с текущей страницы
	for e in elems:
		f.write(e.get_attribute('href')+'\n')	
	# ищем кнопку следующие и если есть - переходим по ней
	try:
		next_button=driver.find_element_by_link_text("Следующие")
	except:
		continue
	count=0
	while next_button:
		if count>=LIMIT_PAGE:
			break
		print next_button.get_attribute('href')
		next_button.click()
		# cсылки на объявления с текущей страницы
		elems_p=driver.find_elements_by_class_name("item-link")
		for el in elems_p:
			f.write(el.get_attribute('href')+'\n')
		try:
			next_button=driver.find_element_by_link_text("Следующие")
			count+=1
		except:
			break	
f.close()


