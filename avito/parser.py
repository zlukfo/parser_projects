# -*- coding: utf-8 -*-
# !!! парсим телефон с объявлений avito (мобильной версии)
from selenium import webdriver
from pyvirtualdisplay import Display 
import codecs
# чтобы работать со страницей без открытия браузера
display = Display(visible=0, size=(800, 600))
display.start()
driver = webdriver.Firefox()

def _getData(link, driver):
	data={}
	data['link']=link
	driver.get(link)
	# номер телефона
	try:
		elem=driver.find_element_by_link_text("Показать номер")
		elem.click()
		data['phone']=elem.get_attribute('href')
	except:
		data['phone']='None'
	# заголовок
	try:
		elem=driver.find_element_by_class_name("single-item-header")
		data['header']=elem.text
	except:
		data['header']='None'
	# цена
	try:
		elem=driver.find_element_by_class_name("price-value")
		data['price']=elem.text
	except:
		data['price']='None'
	# текст
	try:
		elem=driver.find_element_by_class_name("description-preview-wrapper")
		data['text']=elem.text
	except:
		data['text']='None'
	# дата
	try:
		elem=driver.find_element_by_class_name("item-add-date")
		data['date']=elem.text
	except:
		data['date']='None'	
	return data


f=open('remont_stroitelstvo2.txt', 'r')
ff=codecs.open('remont_stroitelstvo_1.csv','w','utf-8')
count=0
for link in f:
	d=_getData(link, driver)
	ff.write('#start#\n%s;%s;%s;%s;%s;%s\n#end#\n' % (d['phone'],d['price'],d['header'],d['text'],d['date'],d['link']))
	count+=1
	print count
ff.close()
f.close()

