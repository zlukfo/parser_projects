# -*- coding: utf-8 -*-
# Сайт irr.ru
# Текущий проект - предложение услуг по ремонту и строительству (парсинг номеров телефонов)
# Скрипт обходит страницы на которых находятся ссылки на объявления и собирает эти ссылки в файл
from xmlIterParser import xmlParser
from utilFunc import getPathList


#--- параметры для конкретного проекта

# имя файла в который будут сохраняться ссылки на объявления
FILENAME_LIST_LINKS='estate_links11.txt'

# генератор ссылок на страницы со списоками объявлений
# каждая ссылка генератора должна давать доступ к списку объявлений длиной не более 1000 (с учетом пагинации)
# поэтому генератор ссылок кодируется исходя из общего количества объявлений в интересующей категории
# (смотрим на сайте) и допустимых для данной категории фильтров
MAX_PRICE=10000000
MIN_PRICE=0
STEP=100000
gen_links=('http://'+region[:-2]+'.irr.ru/real-estate/apartments-sale/'+typ+'/search/boundary_in_rooms='+str(room)+'/price=от%20'+str(min_p)+'%20до%20'+str(min_p+STEP)+'/' for region in open('irr_regions_name.txt', 'r') for typ in ['new', 'secondary'] for room in xrange(6) for min_p in xrange(MIN_PRICE,MAX_PRICE,STEP))

#нужно указать любую страницу со списком объявлений из интересующей категории
base_url='http://yaroslavskaya-obl.irr.ru/real-estate/apartments-sale/'
# и скопировать куки
cookie="_ym_uid=1475779997823789671; rrlpuid=; rrpvid=322289091069251; __gads=ID=23750dd4d2b4da34:T=1475780170:S=ALNI_MYWD2dWtyhmAB8Avm4evhSRrZ3xEA; crtg_rta=; _ga=GA1.2.2049410746.1475779997; irr_region_id=1252; __io_sp=/real-estate/; spUID=14757800037855e566c6c8c.65db6b66; puid=984c91010bd69d0e1fe10bf6de82787c; csid=24b0cbe848cc88fa772e46afbc01bcb9776903aa; una=1; exp_53=47; crtg_rta=; __io_lv=1476330941904; __io_uid_test=17; clicks_queue=[]; _clicks_session_id=196307982318103; __utma=136287977.2049410746.1475779997.1476122863.1476330945.13; __utmc=136287977; __utmz=136287977.1475779997.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utmv=136287977.|2=IP=78.25.122.19=1^3=newsite=1=1; _ym_isad=1; inLanding=http%3A%2F%2Fyaroslavskaya-obl.irr.ru%2F; insdrSV=102; _ga=GA1.3.2049410746.1475779997; rrrbt=; rcuid=5752d4cd6614e3150cbc0339; rrpusid=5752d4cd6614e3150cbc0339; scs=%7B%22t%22%3A1%7D; __io=282c473b0.4b8ee6220_1475814011797; _io_un=13"

#---------------------
links_d=open(FILENAME_LIST_LINKS,'w')
# !!!!! ВАЖНО - если будет выдавать ошибку - попробовать закомментировать  'Accept-Encoding' в header
header={
	'Accept':"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
	#'Accept-Encoding':"gzip, deflate, sdch",
	'Accept-Language':"ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4",
	'Cache-Control':"no-cache",
	'Connection':"keep-alive",
	'Cookie': cookie,
	'Host':base_url.split('/')[2],
	'Pragma':"no-cache",
	'Upgrade-Insecure-Requests':"1",
	'User-Agent':"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36"
}


x=xmlParser(base_url, 'html', header=header)
# имя класса атрибута <а>, который содержит ссылки на каонкретные объявления
LINK_OB_CLASS_NAME='listing__itemTitle js-productListingProductName'
#print x.getTree(get_attrib=True)

path_to_ob_link=getPathList(x.getTree(get_attrib=True), attrval=LINK_OB_CLASS_NAME)[0]


for l in gen_links:
	last_p_n=1
	x.header['Host']=l.split('/')[2]
	# количество страниц с объявлениями по сгенерированному адресу
	for i in x.getData(['html/body/div/div/div/div/div/main/div/div/div/ul/li/a'], source_path=l):
		last_p_n=int(i[1])	
	print "pages - %d link - %s" % (last_p_n, l)
	# постранично обходим адрес и собираем ссылки на объявления
	for i in xrange(0,last_p_n):
		link_suff=''
		if i>0:
			link_suff='page'+str(i+1)+'/'
		for i in x.getData([path_to_ob_link], get_attrib=True, source_path=l+link_suff):
			if 'href' in i[1]:
				links_d.write(i[1]['href']+'\n')

links_d.close()



# общее 
# пагинация на странице списков с объявлениями находится по пути
# 'html/body/div/div/div/div/div/main/div/div/div/ul/li/a'
# узнать можно так (с учетом, что объявлений мало и пагинации может не быть)
#last_p_n=None
#try:
#	print getPathList(x.getTree(get_attrib=True), attrval='pagination__pagesLink')
#except:
#	last_p_n=0

# узнать номер последней страницы можно так
#last_p_n=0
#for i in x.getData(['html/body/div/div/div/div/div/main/div/div/div/ul/li/a']):
#	last_p_n=int(i[1])	
#print last_p_n


