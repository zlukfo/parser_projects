# -*- coding: utf-8 -*-
# Файл содержит функции-утилиты

#--------------парсер xmlParser
# Восстановление пути по заданным параметрам
# tree - Результат работы метода getTree()
def getPathList(tree, tagname='', attrname='', attrval=''):
	paths=[] 
	tl=[[],[],[],[]]
	if not tagname and not attrname and not attrval:
		return paths
	if tagname:
		tl[0]=[i for i in xrange(len(tree[0])) if tree[0][i]==tagname]
	if attrname and len(tree)==3:
		tl[1]=[i for i in xrange(len(tree[2])) for j in tree[2][i] if j[0]==attrname]
	if attrval and len(tree)==3:
		tl[2]=[i for i in xrange(len(tree[2])) for j in tree[2][i] if j[1]==attrval]
	if any(tl):
		tl=list(reduce(lambda res,x: set(res)&set(x), filter(lambda x:x, tl)))
	else:
		tl=[]
	for i in tl:
		path=[i]
		j=0
		while i:
			while i not in tree[1][j]:
				j+=1
			path.append(j)
			i,j=j,0
		path=[tree[0][i] for i in path]
		path.reverse()
		paths.append('/'.join(path))
	return paths

#---------------Результаты парсинга
import re
def splitByCategory(filename, category_list, prefix=''):
	'''
	Function: splitByCategory
	Summary: разделяет файл с данными по категориям. При парсинге категории часто указываются в ссылках. Если входной файл является а) списком ссылок на объявления или б) соостоит из распаренных объявлений, содержашим url на них, то можно разделить этот файл нанесколько в каждом из которых будет информация, соотсетвующая только одной категории 
	Examples: splitByCategory('uslugi3.csv', cat, 'building_')
	Attributes: 
		@param (filename):имя входного файла
		@param (category_list): список категорий. по сути - часть пути url. можно задавать регулярным выражением
		@param (prefix): префикс имени выходных файлов. по умолчанию создаются выходные файлы с именем соответсвующей категории и расширением входного файла. Указав префикс, мы добавляем его в начало имени файла 
	Returns: None
	'''
	ext=filename.split('.')[-1]
	descr=[open(prefix+cat+'.'+ext, 'a') for cat in category_list]
	f=open(filename, 'r')
	for string in f:
		for cat in category_list:
			if re.search(cat, string):
				descr[category_list.index(cat)].write(string)
	return None

import csv
import codecs
def wordStat(csv_in_filename, columns=[0], alphabet='rus', csv_out_filename='word_stat.txt'):
	'''
	Function: wordStat
	Summary: строит частотный словарь слов из файла csv по заданным полям
	Examples: InsertHere
	Attributes: 
		@param (csv_in_filename):InsertHere
		@param (columns) default=[0]: номера столбцов по значениям которых нужно простроить частотный словарь
		@param (alphabet) default='rus': алфавит слов в стобцах можно еще 'lat'
		@param (csv_out_filename) default='word_stat.txt': InsertHere
	Returns: None
	'''
	word_dict={}
	alh={'rus': '[^а-яёА-ЯЁ ]+', 'lat': '[^а-яёА-ЯЁ ]+'}
	r = csv.reader(open(csv_in_filename, 'r'), delimiter=';')
	for s in r:
		s=' '.join([s[i] for i in columns])
		s=re.sub(alh[alphabet].decode('utf-8'), '', s.decode('utf-8')).lower()
		s=[word for word in s.split(' ') if word]
		for word in s:
			if word in word_dict:
				word_dict[word]+=1
			else:
				word_dict[word]=1

	f=codecs.open(csv_out_filename, 'w', 'utf-8')
	for w in sorted(word_dict, key=word_dict.get, reverse=True):
		f.write(w+':'+str(word_dict[w])+'\n')
	f.close()

def getObByWordStat(ob_filename, word_list, output_filename='ob_by_stat.csv'):
	'''
	Function: getObByWordStat
	Summary: Выбирает объявления по заданному частотному словарю (в нем предварительно оставлены только значимые нужные слова) или по массиву заданных слов
	Examples: InsertHere
	Attributes: 
		@param (ob_filename):InsertHere
		@param (word_list):можно задать ссылку на файл-частоотный словарь, а можно массив нужных слов
		@param (output_filename) default='ob_by_stat.csv': InsertHere
	Returns: InsertHere
	'''
	words=word_list if type(word_list)==type([]) else [w.split(':')[0] for w in open(word_stat_filename,'r')]
	f=open(output_filename,'w')
	for ob in open(ob_filename,'r'):
		for w in words:
			if w in ob:
				f.write(ob)
				break
	return None

if __name__=='__main__':
	getObByWordStat('uslugi3.csv',['проект'],'proekt.csv')