# -*- coding: utf-8 -*-
# парсинг телефонов с базы из рук в руки
from xmlIterParser import xmlParser
import re
import base64
import codecs

#----------------------
FILENAME_LIST_LINKS='irr_list_links.txt'
FILENAME_PARSE_DATA='uslugi3.csv'
COOKIE="puid=9a43f94af543ca9a38de32664b5948ce; csid=df330b0358deedca2b173dcfd73c059cf35f0247; una=1; exp_53=47; _ym_uid=1475779997823789671; inLanding=http%3A%2F%2Frostovnadonu.irr.ru%2F; rrlpuid=; rrpvid=322289091069251; __gads=ID=23750dd4d2b4da34:T=1475780170:S=ALNI_MYWD2dWtyhmAB8Avm4evhSRrZ3xEA; _io_un=7; PHPSESSID=3f98512d0b8cb5d1fd72441dcaa45c23; crtg_rta=; _ga=GA1.2.2049410746.1475779997; irr_region_id=1252; _ym_isad=1; insdrSV=76; scs=%7B%22t%22%3A1%7D; crtg_rta=; __io_lv=1475898156054; __io_uid_test=19; __io_sp=/services-business/building/repair/elektrik-advert604574218.html; __utmt=1; __utma=136287977.2049410746.1475779997.1475861767.1475898160.4; __utmb=136287977.2.9.1475898160; __utmc=136287977; __utmz=136287977.1475779997.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utmv=136287977.|2=IP=188.114.5.246=1^3=newsite=1=1; clicks_queue=[]; _clicks_session_id=504774907138198; _ym_visorc_467657=w; rrrbt=; adview=a%3A1%3A%7Bi%3A604574218%3Bb%3A1%3B%7D; _ga=GA1.3.2049410746.1475779997; _gat_UA-19320369-5=1; rcuid=5752d4cd6614e3150cbc0339; rrpusid=5752d4cd6614e3150cbc0339; rr-viewItemId=604574218; rrviewed=604574218; rrlevt=1475898166467"
#----------------------

base_url='http://irr.ru'

header={
	'Accept':"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
	#'Accept-Encoding':"gzip, deflate, sdch",
	'Accept-Language':"ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4",
	'Cache-Control':"no-cache",
	'Connection':"keep-alive",
	'Cookie':COOKIE,
	'Host':"irr.ru",
	'Pragma':"no-cache",
	'Upgrade-Insecure-Requests':"1",
	'User-Agent':"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36"
}


x=xmlParser(base_url, 'html', header=header)

paths=[
	'html/body/div/div/div/div/div/main/div/div/div/div/div/div/div/div/div/div/div',
	'html/body/div/div/div/div/div/main/div/div/div/div/div/h1',
	'html/body/div/div/div/div/div/main/div/div/div/div/div/div',
	'html/body/div/div/div/div/div/main/div/div/div/p'
]
data={
	'phone':"None",
	'price':"None",
	'text':[]
	
}
f=codecs.open(FILENAME_PARSE_DATA,'w',"utf-8")
for link in open(FILENAME_LIST_LINKS, 'r'):
	# !!!! Важная фишка сайта - хост адреса объявления и отсылаемое в запросе значение заголовка Host должны совпадать
	x.header['Host']=link.split('/')[2]
	for i in x.getData(paths, get_attrib=True, source_path=link):
		if type(i[1])==type({}) and 'b' in i[1]:
			data['price']=i[1]['b']
			data['price']=re.sub(r'[^\d]','', data['price'])
		if 'data-phone' in i[1]:
			data['phone']= base64.b64decode(i[1]['data-phone'])
			data['phone']=re.sub(r'[\(\)\- ]*', '', data['phone'])
		if type(i[1])==type(u''):
			data['text'].append(re.sub(r'[\t\n]*| {2:}','',i[1]))
	try:
		data['text']=data['text'][0]+';'+data['text'][-1]
		print data['phone'], data['price'], data['text']
		f.write(data['phone']+';'+data['price']+';'+data['text']+';'+link[:-1]+'\n')
	except:
		pass
	data['text']=[]
	
