# -*- coding: utf-8 -*-
# основная фишка в том, что xml файлв парсятся по sax (в парять загружается на весь файл, а отдельные xml элементы)
# это вроде как 1) ускоряет процесс парсинга 2) дает возможность обрабатывать большие файлы, которые бы в память не поместились
# соглашения по названию функций
# с "_" начинаются служебные, непользователькие функции
# с прописной буквы - функции парсинга XML - файла - (tree, parse)
# с заглавной - функции разбора XML документа конкретного типа (например, банковской выписки)
from lxml import etree
import urllib2
import re
import hashlib
from igraph import *
import simplejson
#from extendHtml import htmlTools
#from extendRssParser import rssParser




# Этот класс содержит необходимые методы для возврашения parse результатов в различных формах
# По умолчанию parse возвращает данные сразу после парсинга очередного тега (SAX) 
# данные возвращаются кортежем 2-х элементов (имя тега, значение)
# Часто нужно скомпоновать несколько тегов, относящихся к одному событию (объекту) в один элемент (например, словарь)
# Это можно понимать как строку таблицы, содержающую значения нескольких полей
# Способов такой компоновки может быть несколько. список, словарь, другие, более сложные ....
# так вот методы этого класса и позволяют проводить такое преобразование
# как использовать класс см ф. parse  
class TransformData():
	def __init__(self, type_resp=None):
		self.type=type_resp
		if type_resp=='json':self.bufer={} # это буфер строки результата - сюда будут накапливаться данные до тех пор пока не начнется следующая порция
		else: self.bufer=None              # следующая порция - когда ключи начнут повторяться 
	def _parseDefault(self, data):
		'''
		Function: служебная функция. Данные отдаются без изменений
		'''
		return data
	def _parseJson(self, data):
		'''
		Function: служебная функция. Выполняет следующее преобразование: Предполагается, что каждое значение
				  data - значение одной ячейки таблицы. А нужно собрать все значения всех ячеек одной строки
				  и только потом вернуть ее целиком. Затем следующую строку и т.д. Данная функция накапливает
				  значения в буфере, пока имя очередной ячейки не повторится (признак того, что данные относятся)
				  уже к другой строке.
				  !!! Важно не забывать, что данные последней строки остаются в буфере. Извлекать их надо вручную
				  через свойство класса bufer
		'''
		if not data[0] in self.bufer:
			self.bufer[data[0]]=data[1]
			return None
		return self.bufer
	# единственная внешняя функция класса
	def parse(self, data):
		'''
		Function: Единственная внешняя функция. Определяет как нужно трансформировать данные 
				  и организует логику транформации
		'''
		if self.type==None: return self._parseDefault(data)
		if self.type=='json': 
			r=self._parseJson(data)
			if r: self.bufer={}	# если вернулся результат - сбрасываем буфер накопления 
			return r or None



class xmlParser():
	"""docstring for ClassName"""
	def __init__(self, source_path='http://lenta.ru/l/r/EX/import.rss', source_type='xml', header=None):
		
		#---здесь подключаем методы классов расширения
		#self.html=htmlTools(self)
		#self.rssParser=rssParser(self)

		#-----
		
		self.source_path=source_path
		self.source_type=source_type
		#self._source_descriptor=None
		self.header=header or {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
       				'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       				'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       				'Accept-Encoding': 'none',
       				'Accept-Language': 'en-US,en;q=0.8',
       				'Connection': 'keep-alive'}
		self._source_descriptor=self._getDescriptor()
	
	#======= СЛУЖЕБНЫЕ ФУНКЦИИ	
	def _getDescriptor(self, source_path=''):
		'''
		Function: _getDesctiptor
		Summary: служебная функция. С помощью регулярного выражения определяет тип источника данных xml, заданный в 
			 в параметре self.source_path. Различает 2 типа источника - интернет адрес или (в остальных случаях)
			 путь к файлу на диске. Используется в пользовательских фунциях tree и parse
		Examples: InsertHere
		Attributes: 
		@param (file_output) default='': InsertHere
		Returns: Если источник не задан - возвращает None. Если задан -
			 возвращает пару - 1) Объект-дескриптор источника 2) имя выходного файла (если при вызове не задан) 
		'''
		path=source_path or self.source_path 	# если источник не указан явно -им выступает путь указанный при создании генератора
		if re.search(ur'^http', path):
			resp=urllib2.Request(path,headers=self.header)
			try:
				resp=urllib2.urlopen(resp)
			except urllib2.HTTPError, e:
				print path, e.code, e.reason
				return None 
			descriptor = StringIO(resp.read())
		else:
			descriptor=self.source_path
		return descriptor

	def _tree(self, context, count, all_schema, get_attrib):		
		'''
		Function: _tree
		Summary: служебная функция. Выполняет непосредственный парсинг источника данных xml для определения его структуры.
			 Источник задается не при вызове данной функции, а косвенно, через пользовательскую функцию tree
			 Парсинг основан на применении пакета lxml. Поэтому в функции tree отпределяется контекст источника
			 xml (указывается путь к источнику и задаются параметры контекста), а затем созданный объект-контекст
			 передается в данную функцию в качестве первого обязательного параметра.			 
		Examples: InsertHere
		Attributes: 
			@param (context): Обязательный параметр - объект-контекст пакета lxml
			@param (count) default=0: не обязательный параметр - количество тегов объекта xml, которое будет 
						пропарсено с начала объекта. Этот параметр нужет потому, что в качестве объекта может быть задан файл
						очень большого размера (как правило содержащий очень большое количество записей). При этом, для того
						чтобы определить структуру источника нет необходимости парсить весь файл, достаточно его небольшую
						первую часть. Эта часть определяется опытным путем, через количество ЛЮБЫХ тегов, встречаемых в ходе
						парсинга. При значении параметра по умолчанию источник парсится полностью
			@param (all_schema) default=False: Необязательный но важный параметр. Определяет подход к парсингу схемы
						таких подходов два 1) (хатактерен для xml источников) предполагает, что на каждом уровне
						вложенности структура данных однородна. поэтому достаточно пропарсить первый элемент, чтобы
						узнать структуру всех остальных с на данном уровне с таким же именем тега. по умолчанию так и делает
						2) (характерен для html). каждый элемент и его стркуктура - уникальны поэтому парсится и включаются
						в дерево ВСЕ элементы
		Returns: возвращает граф-дерево структуры источника в следующем представлении
			 возвращаются два списка одинаковой длины. 1) список вершин (локальных имен тегов) 
			 ПОСЛЕДОВАТЕЛЬНО встречающихся при парсинге источника 2) список, где каждый элемент -
			 список индексов вершин списка (1), указывающих на вершины, являющиеся непосредственными "дочками"
			 вершины расположенной в списке (1) с индексом елемента из списка (2). Если в списке (1) вершина 
			 является конечной (в данном теге находится данные), то под соответсвующим индексом в списке (2)
			 элементом будет пустой массив 
		'''
		vert=[] 		# список имен вершин
		result=[]		# каждый элемент в массиве - список позиций вершин из vert с которой соединяется соответсвующая вершина vert
		attrib=[]
		level_vert=[]
		c=0
		level=-1
		for event, elem in context:
			if event=='start': 
				level+=1
				vert.append(elem.xpath('local-name()'))
				result.append([])
				if get_attrib: attrib.append(elem.attrib.items())
				level_vert.append(level)			
				if not elem.getparent() is None:
					p=elem.getparent().xpath('local-name()')
					# вся схема источника - каждый элемент рассматривается как уникальный
					if all_schema:
						for i in xrange(len(vert)-1,-1,-1):
							if vert[i]==p and level_vert[i]==level-1:
								result[i].append(len(vert)-1)
								break
					# базовая схема - элементы одного уровня с одинаковым тегом рассматриваются
					# как набор однотипных элементов с одинаковой ВЛОЖЕННОЙ структурой
					# ?? поэтому доходя до такого элемента парсится только структура первого ???
					else:
						v=[vert[i] for i in result[vert.index(p)]] 	# все элементы, являющиеся дочерними элементу p, который родитель elem 	
						if not elem.xpath('local-name()') in v:
							result[vert.index(p)].append(len(vert)-1)
						else:		# если такой тэг, выходящий из родительского элемента уже есть - удаляем эту вершину
							vert.pop()
							result.pop()
							if get_attrib: attrib.pop()
							level_vert.pop()
			if event=='end':
				level-=1
				elem.clear()
				while elem.getprevious() is not None and elem.getparent() is not None:
					del elem.getparent()[0]
				if count and c>=count:
					del context
					return (vert, result, attrib)
				c+=1
		del context
		return (vert, result, attrib)

	# Основная функция SAX парсинга при извлечении данных из документа
	# Создает генератор, возвращающий список из извлеченных данных по заданному списку тегов parent_tag
	# извлекаемые данные возвращаются в виде словаря поскольку по структуре могут быть вложенными  
	def _parse(self, context, parent_tag, get_attrib=False):
		'''
		Function: _parse
		Summary:  служебная функция. Вызывается из пользовательской функции parse и непосредственно выполняет
			  извлечение данных из источника. Оба парметра вызова обязательны. Передача им значений 
			  обеспечивается в пользовательской функции parse
		Examples: InsertHere
		Attributes: 
			@param (context): контекс объекта источника. Подробности см в описании функции _tree
			@param (parent_tag): Список тегов элементов источника из которых нужно извлекать данные
							 !!! Важно. поскольку имена тегов на разных ветках дерева структуры
							 источника могут повторяться имя тега можно указывать с частью пути до него
							 Путь можно указывать не весь от корня а только последнюю часть, которая 
							 уникальным образом будет идентифицировать тег. Родительские и дочерние теги в пути
							 разделяются слешем, например "chanel/item/description" 
		Returns: Вазвращает генератор списка, кажный элемент которого - пара 1) локальное имя тега 2) его значение
		'''
		result=[]
		for event, elem in context:
			if event=='start': 
				if elem.getchildren() is None: continue
				# для каждого тега восстанавливаем его путь от корня
				path=''
				e=elem
				while e is not None:
					path=e.xpath('local-name()')+'/'+path
					e=e.getparent()
				path=path[:-1]
				# если заданный в параметре вызова функции тег совпадает с окончанием текущего рассматриваемого тега из xml источника
				# выполняем извлечение данных из него
				if [p for p in parent_tag if re.search(p+'$', path)]: 
					w=list(self._recursive_dict(elem))
					w[0]=path
					yield w
					if get_attrib: yield [path, elem.attrib]
					#yield self._recursive_dict(elem)
			if event=='end':
				elem.clear()
				while elem.getprevious() is not None and elem.getparent() is not None:
					del elem.getparent()[0]
		del context

	#две служебные функции в связке - возвращают значение элемента XML с любой вложенностью в виде словаря
	# для конечных тегов возвращают их данные в виде текста
	def _recursive_dict(self, element):
		el_text=element.text or ''
		el_tail=element.tail or ''
		return element.xpath('local-name()'), self._repeat(map(self._recursive_dict, element)) or el_text+el_tail
	
	def _repeat(self, d):
		dic={}
		for key, val in d:
			if not key in dic:
				dic[key]=val
			else:
				dic[key]=[dic.get(key)] if type(dic[key])!=type([]) else dic.get(key)
				dic[key].append(val)
		return dic

	#=========ПОЛЬЗОВАТЕЛЬСКИЕ ФУНКЦИИ
	def getTree(self, count=0, get_attrib=False):
		'''
		Function: getTree
		Summary: InsertHere
		Examples: InsertHere
		Attributes: 
			@param (count) default=0: парсить весь файл или количество тегов
		Returns: InsertHere
		'''
		self._source_descriptor=self._getDescriptor()
		if not self._source_descriptor: return 		
		if self.source_type=='html':
			context = etree.iterparse(self._source_descriptor, events=('start', "end",), recover=True, html=True)
			vertices, child, attrib=self._tree(context, count, True, get_attrib)
		if self.source_type=='xml':
			context = etree.iterparse(self._source_descriptor, events=('start', "end",))
			vertices, child, attrib=self._tree(context, count, False, get_attrib)
		self.graph_vertices=vertices
		self.graph_child_vertices=child
		self.graph_vertices_attrib=attrib
		self.graph_edges=[]
		for i in xrange(len(self.graph_child_vertices)):
			for j in xrange(len(self.graph_child_vertices[i])):
				self.graph_edges.append((i, self.graph_child_vertices[i][j]))

		return(self.graph_vertices, self.graph_child_vertices, self.graph_vertices_attrib)

	def saveTree2png(self, file_output=None, tree_type=True, width=1200, height=600):
		if not  hasattr(self, 'graph_vertices') or not  hasattr(self, 'graph_child_vertices'): return None

		g = Graph(edges=self.graph_edges, directed=True)
		g.vs['label']=self.graph_vertices
		graphStyle={}

		# граф будет в виде дерева
		if tree_type:
			layout=g.layout_reingold_tilford(root=[0])	#определяет, что график будет выводиться в виде дерева
			graphStyle["layout"]=layout

		graphStyle["vertex_shape"] = 'circle'
		graphStyle["vertex_label_dist"] = 1
		graphStyle["bbox"] = (width,height)

		if re.search(ur'^http',self.source_path):
			if not file_output:
				file_output=self.source_path.split('/')[2]
				if '.' in file_output:
					file_output=file_output.split('.')[-2]
		else:
			if not file_output:
				file_output=self.source_path
		plot(g, file_output+'.png',  **graphStyle)

	def saveTree2svg(self, file_output=None, tree_type=True, width=1200, height=600):
		if not  hasattr(self, 'graph_vertices') or not  hasattr(self, 'graph_child_vertices'): return None

		g = Graph(edges=self.graph_edges, directed=True)
		g.vs['label']=self.graph_vertices
		graphStyle={}

		# граф будет в виде дерева
		if tree_type:
			layout=g.layout_reingold_tilford(root=[0])	#определяет, что график будет выводиться в виде дерева
			graphStyle["layout"]=layout

		if re.search(ur'^http',self.source_path):
			if not file_output:
				file_output=self.source_path.split('/')[2]
				if '.' in file_output:
					file_output=file_output.split('.')[-2]
		else:
			if not file_output:
				file_output=self.source_path
 
		g.write_svg(file_output+'.svg', width=width, height=height, **graphStyle)

	def toD3js(self, vert=0, parent='null', res=[], _flag=True):
		'''
		Function: toD3tree
		Summary: рекурсивная пользовательская функция. Преобразует представление графа дерева, выводимого функцией tree
	         (список вершин+список смежных ребер) в json объект нотации treejson - структуры данных, воспринимаемых
	         библиотекой D3JS. Это удобно для визуализации графа дерева на стороне клиента  
		Examples: InsertHere
		Attributes: 
			@param (vertices): обязательный параметр Список вершин графа
			@param (edges):обязательный параметр Список смежных вершин для каждой вершины
			@param (vert) default=0: !!! для работы рекурсии, при вызове не указывается
			@param (parent) default='null': !!! для работы рекурсии, при вызове не указывается
			@param (res) default=[]: !!! для работы рекурсии, при вызове не указывается
			@param (_flag) default=True: !!! для работы рекурсии, при вызове не указывается
									параметр нужен для того, чтобы обнулять res между пользовательскими запросами
									инача второй и каждый последующий источники данных (их графы-деревья) будут ДОБАВЛЯТЬС
									в res и выводиться в web приложение с накоплением
		Returns: Возвращает структуру графа в нотации treejson
		'''
 		if _flag: res=[]
 		res.append({})
 		l=len(res)-1
 		res[l]['name']=self.graph_vertices[vert]
 		res[l]['parent']=parent
 		if self.graph_vertices_attrib:
 			for k, v in self.graph_vertices_attrib[l]:
 				res[l][k]=v
 		child=[j[1] for j in self.graph_edges if j[0]==vert]
 		if child: 
 			res[l]['children']=[]
 			for i in child:
 				self.toD3js(i, res[l]['name'], res[l]['children'], _flag=False)
 		return res

	def getData(self, parent_tag, source_path='', get_attrib=False, head=0, type_response=None):
		'''
		Function: parse
		Summary: Пользовательская функция. Извлекает данные из указанных тегов источника и возвращает их.
			 !!! Важно. Функция реализует 2 подхода к исвлечению данных. 1) Есть узел (тег) включающий несколько
			 конечных тегов с нужными данными. Тогда для их извлечения достаточно в parent_tag указать имя этого родительского узла 
			 2) Более тонкий - позволяет извлекать данные из тегов находящихся на разных ветках и разных уровнях. Для этого нужно
			 а) Для каждого необъодимого тега указать в parent_tag часть уникального пути б) в параметре type_response
			 указать тип возвращаемых данных - "json" 
		Examples: InsertHere
		Attributes: 
			@param (file_input):обязательный параметр - путь к источнику (файлу или интернет адресу)
			@param (parent_tag):СПИСОК тегов из которых нужно извлечь данные. Элементом списка можно указать локальное
							имя тега (если оно является уникальным в дереве), а можно конечную часть пути по дереву
							уникальным образом идентифицирующую нужный тег (если на разных ветках дерева
							находятся теги с одинаковым названием). При указании пути теги разделяются символом "/"
			@param (head) default=0: указывает - сколько значений нужно извлечь из указанных тегов прежде чем прервать парсинг
								 Параметр полезен в том случае, если источник - большой xml файл, а мы пока точно не знаем
								 данные из каких именно тегов нам нужны. В этом случае полезно провести предварительный парсинг
								 т.е. вывести по заданным тегам первые head значений и посмотреть те ли это данные или нет
			@param (type_response) default=None: Указывает как скомпоновать извлеченные данные перед их возвращением
											 по умолчанию парсер встретив в источнике нужный тег извлекает из него данные и
											 возвращает пару а) имя тега б) данные. Допустимые значения "json"
		Returns: генератор списка данных возвращаемых в компоновке, определенной параметром type_response
		'''
		self._source_descriptor=self._getDescriptor(source_path)
		if not self._source_descriptor: return 
		if self.source_type=='html':
			context = etree.iterparse(self._source_descriptor, events=('start', "end",), recover=True, html=True)
		if self.source_type=='xml':
			context = etree.iterparse(self._source_descriptor, events=('start', "end",))
		a=self._parse(context, parent_tag=parent_tag, get_attrib=get_attrib)
		l=0
		resp=TransformData(type_resp=type_response) 
		for i in a:
			r=resp.parse(i)
			if not r: continue 
			yield r
			if head>0 and l>=head: break
			l+=1
		if resp.bufer: yield resp.bufer

	def restorePathByData(self, data_string='', in_attrib=False):
		if not data_string: return None
		self._source_descriptor=self._getDescriptor()
		if self.source_type=='html':
			context = etree.iterparse(self._source_descriptor, events=('start', "end",), recover=True, html=True)
		if self.source_type=='xml':
			context = etree.iterparse(self._source_descriptor, events=('start', "end",))
		for event, elem in context:
			if event=='start': 
				path=''
				e=elem
				while e is not None:
					path=e.xpath('local-name()')+'/'+path
					e=e.getparent()
				path=path[:-1]
				if  data_string in etree.tostring(elem, method="text",encoding="unicode"):
					return path 
				if in_attrib and [i for i in elem.attrib.values() if data_string in i]:
					return path
			if event=='end':
				elem.clear()
				while elem.getprevious() is not None and elem.getparent() is not None:
					del elem.getparent()[0]
		del context



	#функция расширения класса через подключение дополнительных расширений
	def addExtension(self, alias, module_name, ext_name):
		if alias in dir(self):
			return 'Alias exists'
		# !!__ выполнить проверку существования файла расширения
		ext=__import__(module_name, globals(), locals(), [], -1)
		self.__setattr__(self, alias, ext.rssParser(self)) 



#========== ПРИКЛАДНЫЕ ФУНКЦИИ


# функция считает хеш строки (нужна для сравнения чтобы повторяющиеся строки не попадались)
def hashKey(st=''):
	if type(st)==type(u''): st=st.encode('utf-8')
	m = hashlib.sha256()
	m.update(st)
	return m.hexdigest()


import codecs
import re
if __name__ == "__main__":

	# ИССЛЕДУЕМ СТРУКТУРУ OPEN CORPORA

	pref='https://xn--c1ayft.xn--b1aew.xn--p1ai'
	s=[
		"/aviapereleti/bileti_po_regionu/respubliki/kabardino-balkarskaya",
		"/aviapereleti/bileti_po_regionu/respubliki/karachaevo-cherkesskaya",
		"/aviapereleti/bileti_po_regionu/respubliki/adigeya",
		"/aviapereleti/bileti_po_regionu/respubliki/altai",
		"/aviapereleti/bileti_po_regionu/respubliki/bashkortostan",
		"/aviapereleti/bileti_po_regionu/respubliki/buryatiya",
		"/aviapereleti/bileti_po_regionu/respubliki/dagestan",
		"/aviapereleti/bileti_po_regionu/respubliki/ingushetiya",
		"/aviapereleti/bileti_po_regionu/respubliki/kalmikiya",
		"/aviapereleti/bileti_po_regionu/respubliki/kareliya",
		"/aviapereleti/bileti_po_regionu/respubliki/komi",
		"/aviapereleti/bileti_po_regionu/respubliki/marii_el",
		"/aviapereleti/bileti_po_regionu/respubliki/mordoviya",
		"/aviapereleti/bileti_po_regionu/respubliki/saha_yakutiya",
		"/aviapereleti/bileti_po_regionu/respubliki/alaniya",
		"/aviapereleti/bileti_po_regionu/respubliki/tatarstan",
		"/aviapereleti/bileti_po_regionu/respubliki/tiva",
		"/aviapereleti/bileti_po_regionu/respubliki/hakasiya",
		"/aviapereleti/bileti_po_regionu/respubliki/udmurtiya",
		"/aviapereleti/bileti_po_regionu/respubliki/chechenskaya",
		"/aviapereleti/bileti_po_regionu/respubliki/chuvawiya",
		"/aviapereleti/bileti_po_regionu/krai/altaiskii",
		"/aviapereleti/bileti_po_regionu/krai/zabaikalskii",
		"/aviapereleti/bileti_po_regionu/krai/kamchatskii",
		"/aviapereleti/bileti_po_regionu/krai/krasnodarskii",
		"/aviapereleti/bileti_po_regionu/krai/krasnoyarskii",
		"/aviapereleti/bileti_po_regionu/krai/permskii",
		"/aviapereleti/bileti_po_regionu/krai/primorskii",
		"/aviapereleti/bileti_po_regionu/krai/stavropolskii",
		"/aviapereleti/bileti_po_regionu/krai/habarovskii",
		"/aviapereleti/bileti_po_regionu/goroda/moskva",
		"/aviapereleti/bileti_po_regionu/goroda/sankt_peterburg",
		"/aviapereleti/bileti_po_regionu/avt_okrug/nenez_avt_okr",
		"/aviapereleti/bileti_po_regionu/avt_okrug/ugra",
		"/aviapereleti/bileti_po_regionu/avt_okrug/chukot_okr",
		"/aviapereleti/bileti_po_regionu/avt_okrug/yamalo_nenez_okrug",
		"/aviapereleti/bileti_po_regionu/oblasti/amurskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/arhangelskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/astrahanskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/belgorodskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/bryanskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/vladimirskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/volgogradskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/vologodskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/voronejskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/ivanovskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/irkutskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/kaliningradskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/kalujskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/kemerovskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/kirovskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/kostromskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/kurganskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/kurskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/leningradskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/lipeckaya",
		"/aviapereleti/bileti_po_regionu/oblasti/magadanskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/moskovskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/murmanskayay",
		"/aviapereleti/bileti_po_regionu/oblasti/nijegorodskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/novgorodskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/novosibirskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/omskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/orenburgskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/orlovskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/penzenskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/pskovskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/rostovskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/ryazanskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/samarskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/saratovskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/sahalinskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/sverdlovskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/smolenskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/tambovskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/tverskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/tomskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/tulskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/tumenskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/ulyanovskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/chelyabinskaya",
		"/aviapereleti/bileti_po_regionu/oblasti/yaroslovskaya"

	]

	f = codecs.open('list.txt','w','utf8')
	x=xmlParser(pref+s[0], 'html')
	for l in s:
		x.source_path=pref+l
		data=x.getData(["/table/tbody/tr"])
		for i in data:
			try:
				t='\t'.join(i[1]['td'])
			except:
				pass
			f.write(t.replace('\n', '')+'\n')
	f.close()

	'''
	x=xmlParser('dict.opcorpora.xml', 'xml')
	x.getTree(get_attrib=True)
	x.saveTree2svg(width=3000, height=600)
	'''
	'''
	# парсер базы вакансий
	# 1. строим дерево
	x=xmlParser('OBV_full.xml', 'xml')
	x.getTree(count=10000)
	x.saveTree2svg(width=3000, height=600)
	# 2. проверяем, есть ли в узлах атрибуты со значимыми данными
	x.getTree(count=10000, get_attrib=True)
	x.saveTree2svg(width=3000, height=600)
	# рисунки не отличаются - атрибутов нет
	
	# 3. пробная выборка данных. задача - 
	# определить структуру данных для каждого поля
	prefix='source/vacancies/vacancy/'
	prefix_2='requirement/'
	prefix_3='addresses/address/'
	prefix_4='company/'

	fields=['url', 'creation-date', 'salary', 'currency', 'category/industry', 'job-name', 'employment', 'schedule', 'description', 'duty', 'term/text', 
			prefix_2+'education', prefix_2+'qualification', prefix_2+'experience',
			prefix_3+'location', prefix_3+'lng', prefix_3+'lat',
			prefix_4+'name', prefix_4+'description', prefix_4+'site', 
			prefix_4+'email', prefix_4+'contact-name', prefix_4+'hr-agency', prefix_4+'phone' 
	]
	data=x.getData([prefix+i for i in fields], head=500000)
	data_array=[]
	one_record={}
	for i in data:
		key=i[0].split('/').pop()
		val=i[1]

		if type(val)==type(u''):val=val.encode('utf-8')
		val=re.sub(r'\n|\r|&nbsp|(<[a-zA-Z\/\\\=\" ]+>)', ' ', val)
		val=re.sub(r' +', ' ', val)
		val=val.strip()

		if not key in one_record:
			one_record[key]=val
		else:
			one_record["collections"]=[{"id": "vacancy"}]
			data_array.append(one_record)
			one_record={}
	one_record["collections"]=[{"id": "vacancy"}]
	data_array.append(one_record)
	
	f = codecs.open('file4.json','w','utf8')
	f.write(simplejson.dumps({"vacancies":data_array}, ensure_ascii=False, indent=4))
	f.close()
	'''

		


	'''
			парсер hh.ru (меняем только данные в генераторе ссылок и имя выходного файла)			
	url='http://hh.ru/'
	x=xmlParser(url, 'html')
	x.header={}

	# функция претендует на универсальное применение (своего рода является оберткой)
	res=x.html.getData(['html/head/meta'],   
					threads=10, 
					source_path=['http://hh.ru/vacancy/%s' % str(i) for i in xrange(3000000,5000000,1)], 
					get_attrib=True
				)
	f=open('vacancy_thread_3_5.csv', 'a')
	for i in res:
		f.write(i.encode('utf-8')+'\n')
	'''	
	'''
	x=xmlParser('b.xml', 'xml')
	data=x.getData(['osm/way/tag'], get_attrib=True)
	d={}
	f=open('b.json','w')
	f.write('[\n')
	for i in data:
		if i[1]!='\n':
			try:
				k,v=i[1].values()
			except:
				print i[1]
			k=k.replace(':','_')
			if not k in d:
				d[k]=v
			else:
				f.write(simplejson.dumps(d,  ensure_ascii=False).encode('utf-8')+',\n')
				d={}
	f.write('\n]')
	f.close()
	'''
	'''
	# парсинг в json базы населенный пунктов с сайта http://gis-lab.info/qa/vmap0-settl-rus.html
	x=xmlParser('np.xml', 'xml')
	data=x.getData(['osm/node', 'osm/node/tag'], get_attrib=True)
	d={}
	f=open('a.txt','w')
	f.write('[\n')
	for i in data:
		if len(i[1])>2:
			if d:
				f.write(simplejson.dumps(d,  ensure_ascii=False).encode('utf-8')+',\n')
			d=dict(i[1])
		if len(i[1])==2:
			k,v=i[1].values()
			k=k.replace(':','_')
			d[k]=v
	f.write('\n]')
	f.close()
	'''




